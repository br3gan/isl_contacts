# Readme

![](https://media.giphy.com/media/QBH18aEcPaEm2n9ejW/giphy.gif)

## Introduction
Life is good in our lil research center.

Oh boy, do I remember the good ol' days when you needed to call or email someone and you was searching their telephones on papers or on the internet ending up searching their info in the deep web. Sorry? What say ye? You are still practicing such ungraceful, unholly methods? Alrighty then, I think you're gonna be insterested in this one.

## Install static search
Fooled ya. Nothing is really being installed here, just some aliases and repository clonings.

1) Clone the repository to a path. I will be using the directoy `~/Documents/isl_contacts` for this tutorial, so, if you don't use the same directory, you want to change from mine to yours after you copy:
```sh
    $ cd ~/Documents
    $ git clone https://bitbucket.org/br3gan/isl_contacts.git
```

2) Add the following line to your rc file (.bashrc for bash users or .zshrc for zsh users). You can even use any other file since you have correctly import to the respective .rc file:
```sh
    # Demokritos Contact list static
    find_contacts_static() { grep -i -B1 -A3 $1 ~/Documents/isl_contacts/contacts;}
    alias contacts_static='find_contacts_static'
    # Demokritos Contact list dynamic
    find_contacts_dynamic(){ wd=$(pwd); cd ~/Documents/isl_contacts/contact_list; fzf; cd $wd;}
    alias contacts_dynamic='find_contacts_dynamic'
```

3) Open a new terminal to source the changed .rc file.

## Run
    $ find_contacts_static "part_of_the_name_you_need_to_find_information_about"

For example:

    $ find_contacts_static Thomopoulos
should give you something like this:

    Name: Thomopoulos Stelios
    Mail: scat@iit.demokritos.gr
    Telephone: 3155

#### Tip: Notice that since a simple grep is applied, you don't really need to write the full name. A simple 'Thom' or 'Ste' or even 'opoul' would also do the job.

# Getting interesting
Hm, that was meh. Let's go WHOAAH!

## Install dynamic search
For this part you'll need to have a utility named fuzzy file finder (fzf) installed.

Ubuntu: Clone the repo from the https://github.com/junegunn/fzf

Arch: Install via a pacman front-end, like yaourt:
    `yaourt -S fzf`

Then you need to create the contact list. Go to your install directory, make executable and run the `make_contacts`
```sh
    $ cd ~/Documents/isl_contacts
    $ chmod +x make_contacts
    $ ./make_contacts
```
This will generate a folder with all the contacts as files. That was so *fzf* can do its business.

## Run
Get a drink, sit comfortably and run

    $ find_contacts_dynamic

from a terminal.
Now start typing the name you want and magic things will happen.

## Extra sugar
The names of the aliases are for tutorial sakes. I recommend something more direct and small and cool, like "isl", instead of "find_contacts_dynamic", and "isl_boring" instead of "find_contacts_boring", or something like that.
